#ifndef SHA256_H
#define SHA256_H


#include <string>

class SHA256
{
protected:
    typedef unsigned char uint8; // typedef - defines 'uint8' as an alias for 'unsigned char' type
    typedef unsigned int uint32;
    typedef unsigned long long uint64;

    static const uint32 sha256_k[]; // the order doesn't matter, but for C 'static' should come first
    static const uint32 BLOCK_SIZE = 64;
public:
    void init();
    void update(const uint8 *message, uint32 len);
    void final(uint8 *digest);
    static const uint32 DIGEST_SIZE = 32;

protected:
    void transform(const uint8 *message, uint32 block_nb);
    uint32 m_tot_len;
    uint32 m_len;
    uint8 m_block[128];
    uint32 m_h[8];
};

std::string sha256(std::string input);

#define SHIFTR(x, n)    (x >> n)
#define ROTR(x, n)   ((x >> n) | (x << ((sizeof(x) << 3) - n)))
#define CH(x, y, z)  ((x & y) ^ (~x & z))
#define MAJ(x, y, z) ((x & y) ^ (x & z) ^ (y & z))
#define F1(x) (ROTR(x,  2) ^ ROTR(x, 13) ^ ROTR(x, 22))
#define F2(x) (ROTR(x,  6) ^ ROTR(x, 11) ^ ROTR(x, 25))
#define F3(x) (ROTR(x,  7) ^ ROTR(x, 18) ^ SHIFTR(x,  3))
#define F4(x) (ROTR(x, 17) ^ ROTR(x, 19) ^ SHIFTR(x, 10))
#define UNPACK32(x, str)                 \
{                                             \
    *((str) + 3) = (uint8) ((x)      );       \
    *((str) + 2) = (uint8) ((x) >>  8);       \
    *((str) + 1) = (uint8) ((x) >> 16);       \
    *((str) + 0) = (uint8) ((x) >> 24);       \
}
#define PACK32(str, x)                   \
{                                             \
    *(x) =   ((uint32) *((str) + 3)      )    \
           | ((uint32) *((str) + 2) <<  8)    \
           | ((uint32) *((str) + 1) << 16)    \
           | ((uint32) *((str) + 0) << 24);   \
}
#endif // SHA256_H
