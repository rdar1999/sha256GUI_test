﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    connect (ui->actionOpen, SIGNAL(triggered()), this, SLOT(on_openFile_clicked()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_textEdit_textChanged()
{
    QString text = ui->textEdit->toPlainText();

    std::chrono::steady_clock time;
    auto start = time.now();

    std::string hash = sha256(text.toStdString());

    auto end = time.now();
    auto time_span = static_cast<std::chrono::duration<double>>(end - start);

    std::string s = std::to_string(time_span.count());
    QString tempo = QString::fromStdString(s);
    QString Qhash = QString::fromStdString(hash);

    ui->lineEdit->setText(Qhash);
    ui->labelTime->setText(tempo + "s");
}

void MainWindow::on_pushButton_clicked()
{
    ui->textEdit->clear();
}

void MainWindow::on_openFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
    tr("Open File"), QCoreApplication::applicationDirPath(), tr("All Files (*.*)"));

    #include <cstdio>

    std::FILE * fp = std::fopen(fileName.toStdString().c_str(), "rb");
    std::fseek(fp, 0L, SEEK_END);
    unsigned int fsize = std::ftell(fp);
    std::rewind(fp);

    std::string myString(fsize, 0);
    if (fsize != std::fread(static_cast<void*>(&myString[0]), 1, fsize, fp))
    {
        ui->lineEdit->setText("Error reading file ... ") ;
    }

    std::fclose(fp);

    std::chrono::steady_clock time1;
    auto start1 = time1.now();

    std::string HashFile = sha256(myString);

    auto end1 = time1.now();
    auto time_span1 = static_cast<std::chrono::duration<double>>(end1 - start1);


    std::string s = std::to_string(time_span1.count());
    QString tempo = QString::fromStdString(s);
    QString QhashFile = QString::fromStdString(HashFile);


    ui->lineEdit->setText(QhashFile);
    ui->labelTime->setText(tempo + "s");
}
