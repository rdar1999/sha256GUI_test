#include "mainwindow.h"
#include "sha256.h"
#include <QApplication>
#include <iostream>
#include <string>
#include <chrono>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow window;
    window.show();
    return app.exec();
}
